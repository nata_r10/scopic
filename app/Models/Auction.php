<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Auction extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function product(){
        return $this->belongsTo('App\Models\Product');
    }

    public function bidhistory(){
        return $this->hasMany('App\Models\BidHistory');
    }

    public function autobids(){
        return $this->hasMany('App\Models\AutobidItem');
    }

    public function highestBid(){
        return $this->bidhistory()->with('user')->orderBy('value', 'desc')->limit(1);
    }

    public function addBidhistory($bid, $user){
        $this->bidhistory()->create([
            'user_id' => $user,
            'value' => $bid
        ]);
    }

    public function userAutobid()
    {
        return $this->autobids()->where('user_id', auth()->id())->first();
    }

    public function toogleAutobid(){

        if(!empty($this->userAutobid())){
            $this->userAutobid()->delete();
        }else{
            $this->autobids()->create([
                'user_id' => auth()->id()
            ]);
        }

    }

}
