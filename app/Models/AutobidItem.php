<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AutobidItem extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function auction(){
        return $this->belongsTo('App\Models\Auction');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}
