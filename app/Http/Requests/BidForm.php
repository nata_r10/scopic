<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Auction;

class BidForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => 'required|numeric',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            //If bid is lower than actual highest bid
            if(count($this->route('auction')->highestBid)>0){
                if($this->value <= $this->route('auction')->highestBid[0]->value){
                    $validator->errors()->add('value', 'Bid cant be lower than highest bid.');
                }
            }
        });
    }

}
