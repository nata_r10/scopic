<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Auction;
use App\Http\Requests\BidForm;

class AuctionController extends Controller
{
    private $order;

    public function __construct()
    {
        $this->middleware('auth');
        $this->order = 'asc';
    }

    public function index(Request $request)
    {
        $auctions = Auction::with('product');
        if($request->order){ $this->order = $request->order; }
        return $this->orderElements($auctions);
    }

    public function show($auction)
    {
        return Auction::with('product', 'highestBid')->where('id', $auction)->first();
    }

    public function update(BidForm $request, Auction $auction)
    {
        $auction->addBidhistory($request->value, auth()->id());
        $this->runAutobids($request->value, $auction);
        return $this->show($auction->id);
    }

    public function orderElements($elements)
    {
        $elements = $elements->reorder('min_bid', $this->order)->paginate(10);
        return $elements;
    }

    public function runAutobids($bidValue, $auction)
    {
        foreach($auction->autobids as $autobid){
            if($autobid->user_id != auth()->id() && $bidValue < $auction->max_bid){
                $bidValue++;
                if($config = $autobid->user->configAutobid){
                    if($config->max_amount > 0){
                        $auction->addBidhistory($bidValue, $autobid->user_id);
                        $config->update(['max_amount' => $config->max_amount-1]);
                    }
                }
            }
        }
    }

}
