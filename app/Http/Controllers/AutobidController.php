<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AutobidConfiguration;
use App\Http\Requests\AutoBidForm;

class AutobidController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $autobid = AutobidConfiguration::where('user_id', auth()->id())->with('user')->first();
        return $autobid;
    }

    public function store(AutoBidForm $request)
    {
        $autobid = AutobidConfiguration::where('user_id', auth()->id())->first();
        if($autobid){
            $autobid->update(['max_amount' => $request->max_amount]);
        }else{
            $autobid = AutobidConfiguration::create([
                'user_id' => auth()->id(),
                'max_amount' => $request->max_amount
            ]);
        }
        return $autobid;
    }

    public function disable()
    {
        $autobid = AutobidConfiguration::where('user_id', auth()->id())->with('user')->first();
        $autobid->delete();
    }

}
