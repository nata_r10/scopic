<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Auction;

class BidHistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Auction $auction)
    {
        return $auction->userAutobid();
    }

    public function toogle(Auction $auction)
    {
        $auction->toogleAutobid();
        return $auction->userAutobid();
    }
}
