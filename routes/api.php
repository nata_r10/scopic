<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuctionController;
use App\Http\Controllers\AutobidController;
use App\Http\Controllers\BidHistoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->resource('/auctions', AuctionController::class);
Route::middleware('auth:sanctum')->resource('/autobid', AutobidController::class);
Route::middleware('auth:sanctum')->post("autobid/disable", [AutobidController::class, 'disable']);
Route::middleware('auth:sanctum')->post('/toogle/{auction}', [BidHistoryController::class, 'toogle']);
Route::middleware('auth:sanctum')->get('/toogle/{auction}', [BidHistoryController::class, 'show']);
