## About The Project

This project uses PHP / Laravel on the Back-end and React.js on the Front End.

## To install the project

- Clone repo
- cp .env.example .env
- Configure database and domain in .env
- composer install
- php artisan key:generate
- php artisan config:cache
- php artisan migrate:fresh --seed

## Dummy users

Users have been seeded
- User: user1@user1.com  Password: user1
- User: user2@user2.com  Password: user2


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
