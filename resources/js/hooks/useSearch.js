import React, {useMemo, useState} from 'react'

const useSearch = (items) => {
    const [query, setQuery] = useState('');
    const [filteredItems, setfilteredItems] = useState(items);

    useMemo(() => {
            const result = items.filter(item => {
                return item.product.name.toLowerCase().includes(query);
            });
            setfilteredItems(result);
        },
        [items, query]
    );

    return {query, setQuery, filteredItems}
}

export default useSearch;
