import React, {useState, useCallback} from 'react'

const usePostData = ({API, headers, payload, METHOD}) => {

    const [res, setRes] = useState({data: null, error: null, isLoading: false});
    const [error, setError] = useState(null);
    // You POST method here
    const callAPI = useCallback(() => {
         setRes(prevState => ({...prevState, isLoading: true}));

         axios({
            method: METHOD,
            url: API,
            data: payload
        })
         .then(res => {

            setRes({data: res.data, isLoading: false, error: null});

         }).catch((error) => {
             console.log(error);
            let showError = null;
            if(error.response.status == '422'){
                showError = error.response.data.errors;
            }else{
                showError = error.response.data.message;
            }
            setRes({data: null, isLoading: false, error: showError});

         })
    }, [API, headers, payload, METHOD])
    return [res, callAPI];
}

export default usePostData;
