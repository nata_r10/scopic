import React, {useState, useEffect} from 'react'

const usePaginateState = (API, order) => {

    const [page, setPage] = useState(1);
    const [ element, setElements] = useState([]);
    const [ next, setNext] = useState(1);
    const [ corder, setCorder] = useState(order);

    useEffect( () => {

        axios.get(`${API}?order=${order}&page=${page}`)
        .then((response)=> {

            if(order != corder){
                setElements(response.data.data)
            }else{
                setElements([].concat(element, response.data.data))
            }

            setNext(response.data.next_page_url)
            setCorder(order)

        })
        .catch((error) => {
            this.setState({
                error: error,
                loading: false
            });
        });

    }, [page, order]);

    return {page, element, next, setPage};
};

export default usePaginateState;
