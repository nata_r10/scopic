import {useState, useEffect} from 'react'

const useInitialState = (API) => {

    const [ element, setElements] = useState(null);

    useEffect( () => {

        axios.get(API)
        .then((response)=> {
            setElements(response.data)
        })
        .catch((error) => {
            console.log(error);
        });


    }, []);

    return element;
};

export default useInitialState;
