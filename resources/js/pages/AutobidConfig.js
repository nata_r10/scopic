import React, {useState, useEffect} from 'react'
import Modal from '../components/Modal'
import usePostData from '../hooks/usePostData'

const AutobidConfig = ({isOpen, setOpen}) => {

    const [bid, setBid] = useState('');
    //post action
    const API = `/api/autobid`;
    const [res, apiMethod] = usePostData({
        API: API,
        headers: {ContentType: 'text/plain'},
        payload: {max_amount: bid},
        METHOD: 'post'
    });

    const [resGet, apiMethodGet] = usePostData({
        API: API,
        headers: {ContentType: 'text/plain'},
        payload: null,
        METHOD: 'get'
    });

    const [resDel, apiMethodDel] = usePostData({
        API: '/api/autobid/disable',
        headers: {ContentType: 'text/plain'},
        payload: {max_amount: bid},
        METHOD: 'post'
    });

    useEffect( () => {
        setBid('');
        apiMethodGet();
    }, [res, resDel, isOpen]);

    return(
        <Modal isOpen={isOpen} onCloseModal={() => setOpen(false)} size="lg" titulo="Auto Bid Configuration">

            {resGet.data && (
                <h3 className="text-center text-success">
                    Autobid amount configured: ${resGet.data.max_amount}
                </h3>
            )}

            <h2 className="text-center">Please enter the autobid ammount.</h2>

            <div className="px-5">
                <input
                    className={`form-control ${res.error ? (res.error.max_amount && 'is-invalid') : 'valid'} `}
                    onChange={(e) => setBid(e.target.value)}
                    value={bid}
                    type="text" name="max_amount"
                />
            </div>
            {res.error && <div className="text-danger text-center"> {res.error.max_amount} </div> }

            <button onClick={() => apiMethod()} className="btn btn-dark btn-bid mt-4">ENABLE</button>

            {resGet.data && (
                <h3 className="text-center text-success">
                    <button onClick={() => apiMethodDel()} className="btn btn-danger btn-bid mt-4">DISABLE</button>
                </h3>
            )}

        </Modal>
    )

}

export default AutobidConfig;
