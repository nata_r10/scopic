import React from 'react'
import Container from '../components/Container'
import Title from '../components/Title'
import ItemDescription from '../components/detail/ItemDescription'
import BidStatus from '../components/detail/BidStatus'
import useInitialState from '../hooks/useInitialState'

const DetailContainer = (props) => {

    // Random component
    const Completionist = () => <span>Auction Ended!</span>;
    // Renderer callback with condition
    const renderer = ({ days, hours, minutes, seconds, completed }) => {
      if (completed) {
        // Render a complete state
        return <Completionist />;
      } else {
        // Render a countdown
        return (
          <span>
            {days} days {hours}:{minutes}:{seconds}
          </span>
        );
      }
    };

    const API = `/api/auctions/${props.match.params.itemId}`;
    const item = useInitialState(API);

    return (
        <Container>

            {item &&
                <Title title={item.product.name} />
            }

            <div className="row">
                <div className="col-sm-12 col-md-8">
                    {item &&
                        <ItemDescription item={item} />
                    }
                </div>
                <div className="col-sm-12 col-md-4">
                    {item &&
                        <BidStatus item={item} renderer={renderer} />
                    }
                </div>
            </div>

        </Container>
    );
}

export default DetailContainer;
