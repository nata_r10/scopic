import React, {useState} from 'react'
import Container from '../components/Container'
import Title from '../components/Title'
import { useHistory } from "react-router-dom";

const HomeContainer = () => {

    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const login = () => {
        axios.get('/sanctum/csrf-cookie').then(response => {
            axios.post('/login', {
                email: email,
                password: password
            }).then(response => {
                history.push('/home');
            });
        });
    }

    return (
    <Container>

        <Title title="Login" />

        <div className="text-center">
            <h4>Dummy Users</h4>
            <h5>user: user1@user1.com  /  password: user1</h5>
            <h5>user: user2@user2.com  /  password: user2</h5>
        </div>

        <form>

            <div className="form-group row">
                <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Email</label>

                <div className="col-md-6">
                    <input
                        id="email" type="email" className="form-control" name="email"
                        required autoComplete="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)} />
                </div>
            </div>

            <div className="form-group row">
                <label htmlFor="password" className="col-md-4 col-form-label text-md-right">Password</label>

                <div className="col-md-6">
                    <input id="password" type="password" className="form-control" name="password"
                        required autoComplete="new-password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                     />
                </div>
            </div>

            <div className="form-group row mb-0">
                <div className="col-md-6 offset-md-4">
                    <button type="button" className="btn btn-primary" onClick={() => login()}>
                        Login
                    </button>
                </div>
            </div>
        </form>


    </Container>
    );
}

export default HomeContainer;
