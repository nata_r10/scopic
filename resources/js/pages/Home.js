import React, {useMemo, useState, useEffect} from 'react'
import ItemContainer from '../components/ItemContainer'

const Home = ({items, next, loadMore}) => {

    return (
    <React.Fragment>
        <div className="row">
        {items.length > 0 && (
            <React.Fragment>
                {items.map(item =>
                    <ItemContainer
                        key={item.id}
                        id={item.id}
                        image={item.product.image}
                        title={item.product.name}
                        range={`Bid Range: ${item.product.appraisal}`}
                    />
                )}
            </React.Fragment>
        ) || (
            <h3 className="text-center m-4">No items match your search</h3>
        )}

        </div>

        <hr />

        {next &&
        <div className="text-center">
            <button className="btn btn-outline-dark mt-4" onClick={loadMore}>
                Load More
            </button>
        </div>
        }
    </React.Fragment>
    );
}

export default Home;
