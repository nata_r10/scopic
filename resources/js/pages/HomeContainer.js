import React, {useState} from 'react'
import Container from '../components/Container'
import Title from '../components/Title'
import SearchControls from '../components/SearchControls'
import usePaginateState from '../hooks/usePaginateState'
import useSearch from '../hooks/useSearch'
import Home from './Home'

const HomeContainer = () => {

    //Set the order. Default: asc
    const [order, setOrder] = useState('asc');
    const API = `/api/auctions`;

    //get paginated auctions
    const {page, element, next, setPage} = usePaginateState(API, order);

    //Filter for search
    const {query, setQuery, filteredItems} = useSearch(element);

    return (
    <Container>

        <Title title="Check our items in auction" />

        <SearchControls
            onChange={(e) => {setQuery(e.target.value)}}
            query={query}
            sort={setOrder}
        />

        <Home items={filteredItems} next={next} loadMore={() => setPage(page+1)} />

    </Container>
    );
}

export default HomeContainer;
