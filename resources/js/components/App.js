import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import HomeContainer from '../pages/HomeContainer'
import AuthContainer from '../pages/AuthContainer'
import DetailContainer from '../pages/DetailContainer'
import Featured from './Featured';

import Nav from './Nav'
import Footer from './Footer'


function App() {
    return (
        <div>
        <BrowserRouter>
            <Nav />
            <Featured />
            <Switch>
                <Route exact path="/" component={AuthContainer} />
                <Route exact path="/login" component={AuthContainer} />
                <Route exact path="/home" component={HomeContainer} />
                <Route exact path="/detail/:itemId" component={DetailContainer} />
            </Switch>
            <Footer />
        </BrowserRouter>

        </div>
    );
}

export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
