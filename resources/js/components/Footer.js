import React from 'react'

const Footer = () =>
<div className="container-fluid footer">
    <div className="row mt-5">
        <div className="col-md-12 py-5 text-center">
            <h3>{process.env.MIX_APP_NAME}</h3>
            <small>All Rights Reserved</small>
        </div>
    </div>
</div>

export default Footer;
