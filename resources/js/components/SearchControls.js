import React from 'react'

const SearchControls = ({onChange, query, sort}) =>
<div className="row">
    <div className="col-sm-12 col-md-10">
        <input onChange={onChange} value={query} type="text" className="form-control" placeholder="Search" />
    </div>
    <div className="col-sm-12 col-md-2">
        <div className="dropdown">
            <button className="btn btn-dark dropdown-toggle btn-sort" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Sort By
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <button onClick={() => sort('asc')} className="dropdown-item">Lower Price</button>
                <button onClick={() => sort('desc')} className="dropdown-item">Higher Price</button>
            </div>
        </div>
    </div>
</div>

export default SearchControls;
