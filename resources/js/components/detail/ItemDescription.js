import React from 'react'
import Title from '../Title';

const ItemDescription = ({item}) => {

    return (
        <React.Fragment>
            <img src={item.product.image}
                className="img-fluid mb-4" alt="Responsive image" />

            <Title title="Description" />

            <div className="info-container p-2">
                <h4>{item.product.excerpt}</h4>
                <h5>Appraisal: {item.product.appraisal}</h5>
            </div>
            <pre className="description mt-4">
                {item.product.description}
            </pre>
        </React.Fragment>
    );
}

export default ItemDescription;
