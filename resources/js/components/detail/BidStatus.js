import React, {useState, useEffect} from 'react'
import BidInput from './BidInput'
import Countdown from "react-countdown";
import usePostData from '../../hooks/usePostData'
import ConfigAutobid from './ConfigAutobid'

const BidStatus = ({item, renderer}) => {

    const [endtime, setEndtime] = useState(true);
    const [ref, setRef] = useState(false);

    //post action
    const API = `/api/toogle/${item.id}`;
    const [res, apiMethod] = usePostData({
        API: API,
        headers: {ContentType: 'text/plain'},
        payload: null,
        METHOD: 'post'
    });
    const [resGet, apiMethodGet] = usePostData({
        API: API,
        headers: {ContentType: 'text/plain'},
        payload: null,
        METHOD: 'get'
    });
    const [resAuc, apiMethodAuc] = usePostData({
        API: `/api/auctions/${item.id}`,
        headers: {ContentType: 'text/plain'},
        payload: null,
        METHOD: 'get'
    });

    useEffect( () => {
        apiMethodGet();
        apiMethodAuc();
    }, [res]);

    return (
        <div className="card">
            <div className="card-body text-center">
                <h4 className="card-title">Bidding Status</h4>
                <h5>Time left</h5>
                <h3>
                    <span className="badge badge-secondary">
                        <Countdown
                            date={item.end_date} renderer={renderer}
                            onComplete={() => setEndtime(false)}
                            ref={(countdown) => setRef(countdown)}
                        />
                    </span>
                </h3>

                <small>Ends: {item.end_date}</small>

                {ref && resAuc.data &&
                    <React.Fragment>
                        {endtime && !ref.isCompleted() && (
                            <React.Fragment>
                                <hr />
                                <BidInput item={item} />
                                <hr />

                                <ConfigAutobid />

                                <small className="text-success">{`${resGet.data ? 'Autobidding Enabled' : '' }`}</small>
                                <button
                                    className={`btn btn-bid ${resGet.data ? 'btn-danger' : 'btn-info' }`}
                                    onClick={() => apiMethod()}>
                                    {`${resGet.data ? 'DISABLE' : 'ENABLE' } AUTO BIDDING`}
                                </button>
                            </React.Fragment>
                        ) || (
                            <React.Fragment>
                                <hr />
                                {resAuc.data.highest_bid[0] && (
                                    <div>
                                        <h3>The highest bid was:</h3>
                                        <h4>${resAuc.data.highest_bid[0].value}</h4>
                                        <h3>Placed by</h3>
                                        <h4>{resAuc.data.highest_bid[0].user.name}</h4>
                                    </div>
                                )
                                ||
                                (
                                    <h3>No bids</h3>
                                )}
                            </React.Fragment>
                        )}
                    </React.Fragment>
                }

            </div>
        </div>
    )
}


export default BidStatus;
