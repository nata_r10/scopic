import React, {useState, useEffect} from 'react'
import usePostData from '../../hooks/usePostData'

const BidInput = ({item}) => {

    const [bid, setBid] = useState('');

    //post action
    const API = `/api/auctions/${item.id}`;
    const [res, apiMethod] = usePostData({
        API: API,
        headers: {ContentType: 'text/plain'},
        payload: {value: bid},
        METHOD: 'put'
    });

    //get auction item
    const [resGet, apiGetMethod] = usePostData({
        API: API,
        headers: {ContentType: 'text/plain'},
        payload: null,
        METHOD: 'get'
    });

    useEffect( () => {
        setBid('');
        apiGetMethod();
    }, [res]);


    return (

        <React.Fragment>

        <h4 className="card-title">Highest Bid</h4>
        {resGet.data && (
            <h3>
                {resGet.data.highest_bid[0] ? `$${resGet.data.highest_bid[0].value}` : 'No bids yet'}
            </h3>
        )}
        <input
            className={`form-control ${res.error ? (res.error.value && 'is-invalid') : 'valid'} `}
            onChange={(e) => setBid(e.target.value)}
            value={bid}
            placeholder="Enter your bid"
            type="text" name="bid"
        />
        {res.error && <div className="invalid-feedback"> {res.error.value} </div> }

        <button onClick={() => apiMethod()} className="btn btn-dark btn-bid mt-4">BID NOW</button>

        </React.Fragment>
    )
}
export default BidInput;
