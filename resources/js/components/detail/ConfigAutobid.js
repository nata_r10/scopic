import React, {useState} from 'react'
import AutobidConfig from '../../pages/AutobidConfig'
import Gear from './Gear'

const ConfigAutobid = () => {

    const [open, setOpen] = useState(false);

    return (
        <div>

            <button
                className="btn btn-dark btn-auto btn-bid d-inline mb-3"
                onClick={() => setOpen(true)}
            >
                <Gear />
                AUTO BIDDING
            </button>
            <AutobidConfig isOpen={open} setOpen={setOpen} />

        </div>
    )
}

export default ConfigAutobid;
