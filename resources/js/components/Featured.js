import React from 'react'

const Featured = () =>
<div className="text-center p-4 featured mb-4">
  <h1>Benefit Auctions <small>We can contribute!</small></h1>
</div>

export default Featured;
