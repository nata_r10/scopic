import React from 'react'
import { useHistory } from "react-router-dom";
import {Link} from 'react-router-dom'

const Nav = (props) => {

    const history = useHistory();

    const logout = () => {

        axios.get('/sanctum/csrf-cookie').then(response => {
            axios.post('/logout').then(response => {
                history.push('/');
            });
        });
    }

    return(
        <nav className="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div className="container">

                <div className="navbar-collapse pt-2 text-center" id="navbarSupportedContent">

                    <h3 className="font-weight-bold">{process.env.MIX_APP_NAME}</h3>

                    <ul className="ml-auto mb-0 p-0">

                    <Link to="/home"><h5 className="btn d-inline font-weight-bold text-primary">Home</h5></Link>

                    <button
                        className="btn d-inline font-weight-bold"
                        onClick={() => logout()}
                    >
                        Logout
                    </button>

                    </ul>
                </div>

            </div>
        </nav>
    )

}

export default Nav;
