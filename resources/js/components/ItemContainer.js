import React from 'react'
import {Link} from 'react-router-dom';

const ItemContainer = ({id, image, title, range}) =>
<div className="col-sm-12 col-md-4 mt-4">
    <div className="item-container">
        <Link to={`/detail/${id}`}>
            <img src={image} className="img-fluid mb-2" alt="Responsive image" />
        </Link>
        <div className="info-container p-2">
            <h4>{title}</h4>
            <h5>{range}</h5>
            <Link to={`/detail/${id}`} className="btn btn-dark btn-bid mt-4">BID NOW</Link>
        </div>
    </div>
</div>

export default ItemContainer;
