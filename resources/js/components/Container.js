import React from 'react'

const Container = (props) => 
<div className="container">
    <div className="row justify-content-center">
        <div className="col-md-12">
            {props.children}
        </div>
    </div>
</div>

export default Container;