import React from 'react'
import ReactDOM from 'react-dom'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

const ModalGeneral = ({children, isOpen, onCloseModal, size, titulo}) => {

    return (

        ReactDOM.createPortal(

            <Modal show={isOpen} onHide={onCloseModal}
                size={size}
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className="text-center" closeButton>
                <Modal.Title> {titulo} </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {children}

                </Modal.Body>
            </Modal>

        , document.getElementById('modal') )

    );
}

export default ModalGeneral;
