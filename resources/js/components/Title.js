import React from 'react'

const Title = ({title}) => 
<div>
    <h2>
        {title}
    </h2>
    <hr />
</div>

export default Title;