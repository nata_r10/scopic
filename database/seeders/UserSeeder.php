<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Test 1',
            'email' => 'user1@user1.com',
            'password' => Hash::make('user1'),
        ]);
        $user = User::create([
            'name' => 'Test 2',
            'email' => 'user2@user2.com',
            'password' => Hash::make('user2'),
        ]);
    }
}
