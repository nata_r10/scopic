<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Auction;

class AuctionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Product::all() as $product){

            Auction::create([
                'product_id' => $product->id,
                'start_date' => date("Y-m-d h:i", time()),
                'end_date' => date("Y-m-d h:i", time() + rand(86400, 106400)),
                'min_bid' => rand(100, 800),
                'max_bid' => rand(1500, 3000),
            ]);

        }
    }
}
